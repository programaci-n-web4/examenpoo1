/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

/**
 *
 * @author emoru
 */

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.Docente;

public class Controlador implements ActionListener {
    
    private JTextField jTextField1;
    private JTextField jTextField2;
    private JTextField jTextField3;
    private JTextField jTextField4;
    private JTextField jTextField5;
    private JTextField jTextField6;
    private JTextField jTextField7;
    private JTextField jTextField8;
    private JTextField jTextField9;
    private JSpinner jSpinner1;
    private JSpinner jSpinner2;
    private JButton jButton1;
    private JButton jButton2;
    private JButton jButton3;
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
    // Habilitar los campos de texto
    jTextField1.setEnabled(true);
    jTextField2.setEnabled(true);
    jTextField3.setEnabled(true);
    jSpinner1.setEnabled(true);
    jTextField4.setEnabled(true);
    jTextField5.setEnabled(true);

    // Habilitar el botón de Guardar
    jButton2.setEnabled(true);
}
    
   private void jButtonGuardarActionPerformed(java.awt.event.ActionEvent evt) {                                               
    // Verificar que todos los campos estén llenos
    if (jTextField1.getText().isEmpty() || jTextField2.getText().isEmpty() || jTextField3.getText().isEmpty() ||
        jTextField4.getText().isEmpty() || jTextField5.getText().isEmpty()) {
       
    } else {
        // Capturar los datos ingresados
        String numDocente = jTextField1.getText();
        String nombre = jTextField2.getText();
        String domicilio = jTextField3.getText();
        String nivel = jSpinner1.getValue().toString();
        double pagoPorHoraBase = Double.parseDouble(jTextField4.getText());
        int horasImpartidas = Integer.parseInt(jTextField5.getText());
        // Crear el objeto Docente y asignar los valores capturados


        // Guardar el objeto Docente en alguna estructura de datos o en una variable de clase si es necesario

        // Habilitar el botón de "Mostrar"
        jButton3.setEnabled(true);

        
    }
}

   private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {                                         
    // Obtener los valores de entrada
    String nivel = (String) jSpinner1.getValue();
    double pagoBase = Double.parseDouble(jTextField4.getText());
    int horasImpartidas = Integer.parseInt(jTextField5.getText());
    
    // Calcular el pago por horas impartidas
    double pagoHorasImpartidas = pagoBase * horasImpartidas;
    
    // Calcular el pago por bono según el nivel
    double pagoBono = 0.0;
    if (nivel.equals("Licenciatura")) {
        pagoBono = 1000.0;
    } else if (nivel.equals("Maestro en Ciencias")) {
        pagoBono = 2000.0;
    } else if (nivel.equals("Doctor")) {
        pagoBono = 3000.0;
    }
    
    // Obtener el número de hijos
    int numHijos = Integer.parseInt((String) jSpinner2.getValue());
    
    // Calcular el descuento por impuestos según el número de hijos
    double descuentoImpuestos = 0.0;
    if (numHijos <= 2) {
        descuentoImpuestos = 0.05;
    } else if (numHijos <= 4) {
        descuentoImpuestos = 0.1;
    } else {
        descuentoImpuestos = 0.15;
    }
    
    // Calcular el total a pagar
    double totalPagar = pagoHorasImpartidas + pagoBono;
    totalPagar -= totalPagar * descuentoImpuestos;
    
    // Mostrar los resultados en los campos de texto
    jTextField6.setText(Double.toString(pagoHorasImpartidas));
    jTextField7.setText(Double.toString(pagoBono));
    jTextField8.setText(Double.toString(descuentoImpuestos));
    jTextField9.setText(Double.toString(totalPagar));
}

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }


    
    
}
